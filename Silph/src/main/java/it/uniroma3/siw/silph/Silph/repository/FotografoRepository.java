package it.uniroma3.siw.silph.Silph.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.silph.Silph.model.Fotografo;

public interface FotografoRepository extends JpaRepository<Fotografo, Long> {
	
    List<Fotografo> findByEmail(String email);
    List<Fotografo> findByNome(String nome);
    List<Fotografo> findByCognome(String cognome);
    List<Fotografo> findByEmailAndNomeAndCognome(String email, String nome, String cognome);

}
