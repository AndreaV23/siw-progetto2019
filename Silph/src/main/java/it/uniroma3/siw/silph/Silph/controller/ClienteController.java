package it.uniroma3.siw.silph.Silph.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.silph.Silph.model.Cliente;
import it.uniroma3.siw.silph.Silph.model.Cliente;
import it.uniroma3.siw.silph.Silph.model.Ordine;
import it.uniroma3.siw.silph.Silph.model.Cliente;
import it.uniroma3.siw.silph.Silph.service.*;
import it.uniroma3.siw.silph.Silph.validator.*;

public class ClienteController {
	
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private OrdineService ordineService;
    @Autowired
    private ClienteValidator validator;
	
	
    @RequestMapping("/completaOrdine")
    public String addCliente(@Valid @ModelAttribute("ordine") Ordine ordine, Model model) {
    	model.addAttribute("ordine", ordine);
        model.addAttribute("cliente", new Cliente());
        return "clienteForm";
    }

    @RequestMapping(value = "cliente", method = RequestMethod.POST)
    public String newCliente(@Valid @ModelAttribute("cliente") Cliente cliente, @Valid @ModelAttribute("ordine") Ordine ordine, 
    									Model model, BindingResult bindingResult) {
    	
        this.validator.validate(cliente, bindingResult);
        
        if (this.clienteService.alreadyExists(cliente)) {
            //cliente.addOrdineSvolto(ordine);
            //ordine = this.ordineService.save(ordine);
            cliente = this.clienteService.save(cliente);
            model.addAttribute("msg", "Ordine Completato");
            return "redirect:/";
        }
        else {
            if (!bindingResult.hasErrors()) {
                cliente = this.clienteService.save(cliente);
                cliente.addOrdineSvolto(ordine);
                ordine = this.ordineService.save(ordine);
                cliente = this.clienteService.save(cliente);
                model.addAttribute("msg", "Ordine Completato");
                return "redirect:/";
            }
        }
        return "clienteForm";
    }


}
