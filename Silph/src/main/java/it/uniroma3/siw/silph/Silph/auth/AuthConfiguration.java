package it.uniroma3.siw.silph.Silph.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.sql.DataSource;

/**
 * The AuthConfiguration is a Spring Security Configuration.
 * It extends WebSecurityConfigurerAdapter, meaning that it provides the settings for Web security.
 */
@Configuration
@EnableWebSecurity
public class AuthConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * A reference to the environment of properties defined through the application.properties file.
     * It is automatically wired before at launch.
     */
    @Autowired
    private Environment environment;

    /**
     * The datasource where to find users in our application.
     * It is a bean (meaning that it is only initialized once)
     */
    private DataSource dataSource;

    /**
     * The configure method is the main method in the AuthConfiguration.
     * it
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/**", "/index").permitAll()
                    .antMatchers(HttpMethod.POST, "/**", "/index").permitAll()
                    .antMatchers(HttpMethod.GET, "/funz/**").hasAnyAuthority("FUNZ")
                    .antMatchers(HttpMethod.POST, "/funz/**").hasAnyAuthority("FUNZ")
                    .anyRequest().authenticated()
                    .and().formLogin().loginPage("/login").defaultSuccessUrl("/funz/indexLoggato")
                    .and().logout().logoutUrl("/performlogout")
                    .logoutSuccessUrl("/")
            	    .and()
            	    .exceptionHandling().accessDeniedPage("/error");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(this.buildDatasource())
                .authoritiesByUsernameQuery("SELECT email, role FROM funzionario WHERE email=?")
                .usersByUsernameQuery("SELECT email, password, 1 as enabled FROM funzionario WHERE email=?");
    }

    @Bean
    DataSource buildDatasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(environment.getProperty("spring.datasource.url"));
        dataSource.setUsername(environment.getProperty("spring.datasource.username"));
        dataSource.setPassword(environment.getProperty("spring.datasource.password"));
        return dataSource;
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

