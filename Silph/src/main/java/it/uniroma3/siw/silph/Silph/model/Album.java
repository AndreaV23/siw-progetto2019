package it.uniroma3.siw.silph.Silph.model;

import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Album {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(unique = true, nullable = false)
	private String titolo;
	
	@OneToMany(mappedBy="album")
	private List<Fotografia> fotografie;
	
	@ManyToOne
	private Fotografo fotografo;
	
	public boolean add(Fotografia e) {
		e.setAlbum(this);
		e.setFotografo(this.fotografo);
		return fotografie.add(e);
	}

	public boolean remove(Object o) {
		return fotografie.remove(o);
	}

	public Fotografia get(int index) {
		return fotografie.get(index);
	}

	public Album() {}
	
	public Album(String titolo) {
		this.titolo = titolo;
		this.fotografie = new ArrayList<Fotografia>();
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitolo() {
		return this.titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public List<Fotografia> getFotografie() {
		return this.fotografie;
	}

	public void setFotografie(List<Fotografia> fotografie) {
		this.fotografie = fotografie;
	}

	public Fotografo getFotografo() {
		return this.fotografo;
	}

	public void setFotografo(Fotografo fotografo) {
		this.fotografo = fotografo;
	}

}
