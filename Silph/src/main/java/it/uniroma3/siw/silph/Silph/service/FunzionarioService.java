package it.uniroma3.siw.silph.Silph.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.Funzionario;
import it.uniroma3.siw.silph.Silph.repository.FunzionarioRepository;


@Transactional
@Service
public class FunzionarioService {

	@Autowired
	private FunzionarioRepository funzionarioRepository;

	public Funzionario save(Funzionario arg0) {
		return funzionarioRepository.save(arg0);
	}

	public void delete(Funzionario arg0) {
		funzionarioRepository.delete(arg0);
	}

	public List<Funzionario> findAll() {
		return (List<Funzionario>) funzionarioRepository.findAll();
	}

	public List<Funzionario> findByEmail(String email) {
		return funzionarioRepository.findByEmail(email);
	}

	public Funzionario findById(Long id) {
		Optional<Funzionario> funzionario = this.funzionarioRepository.findById(id);
		if (funzionario.isPresent()) 
			return funzionario.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Funzionario funzionario) {
		List<Funzionario> funzionarios = this.funzionarioRepository.findByEmailAndNomeAndCognome(funzionario.getEmail(), funzionario.getNome(), funzionario.getCognome());
		if (funzionarios.size() > 0)
			return true;
		else 
			return false;
	}
	
	public Funzionario getCorrente() {
		Authentication auth= SecurityContextHolder.getContext().getAuthentication();
		return this.findByEmail(auth.getName()).get(0);
	}
	
}
