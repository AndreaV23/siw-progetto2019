package it.uniroma3.siw.silph.Silph.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.Album;
import it.uniroma3.siw.silph.Silph.repository.AlbumRepository;


@Transactional
@Service
public class AlbumService {

	@Autowired
	private AlbumRepository albumRepository;

	public Album save(Album arg0) {
		return albumRepository.save(arg0);
	}

	public void delete(Album arg0) {
		albumRepository.delete(arg0);
	}

	public List<Album> findAll() {
		return (List<Album>) albumRepository.findAll();
	}

	public List<Album> findByTitolo(String titolo) {
		return albumRepository.findByTitolo(titolo);
	}

	public Album findById(Long id) {
		Optional<Album> album = this.albumRepository.findById(id);
		if (album.isPresent()) 
			return album.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Album album) {
		List<Album> albums = this.albumRepository.findByTitolo(album.getTitolo());
		if (albums.size() > 0)
			return true;
		else 
			return false;
	}	
	
}
