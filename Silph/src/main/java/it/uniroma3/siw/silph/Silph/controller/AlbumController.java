package it.uniroma3.siw.silph.Silph.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.silph.Silph.validator.*;
import it.uniroma3.siw.silph.Silph.model.*;
import it.uniroma3.siw.silph.Silph.service.*;

@Controller
public class AlbumController {
	

	@Autowired
    private AlbumService albumService;
	@Autowired
    private FotografoService fotografoService;
    @Autowired
    private AlbumValidator validator;
    
    @RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
    public String getAlbum(@PathVariable("id") Long id, Model model) {
        model.addAttribute("album", this.albumService.findById(id));
    	return "showAlbum";
    }
    
    @RequestMapping("/albums")
    public String albums(Model model) {
        model.addAttribute("albums", this.albumService.findAll());
        return "albumList";
    }

    @RequestMapping("/funz/addAlbum")
    public String addAlbum(Model model) {
        model.addAttribute("fotografos", this.fotografoService.findAll());
        model.addAttribute("album", new Album());
        return "albumForm";
    }
    
    @RequestMapping(value = "/funz/album", method = RequestMethod.POST)
    public String newAlbum(@Valid @ModelAttribute("album") Album album, 
    									Model model, BindingResult bindingResult) {
        this.validator.validate(album, bindingResult);
        
        if (this.albumService.alreadyExists(album)) {
            model.addAttribute("exists", "Album already exists");
            return "/funz/albumForm";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.albumService.save(album);
                model.addAttribute("albums", this.albumService.findAll());
                return "albumList";
            }
        }
        return "/funz/albumForm";
    }

}
