package it.uniroma3.siw.silph.Silph.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.silph.Silph.model.Ordine;

public interface OrdineRepository extends JpaRepository<Ordine, Long> {


}
