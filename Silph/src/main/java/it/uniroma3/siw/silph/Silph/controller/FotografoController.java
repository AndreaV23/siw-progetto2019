package it.uniroma3.siw.silph.Silph.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.silph.Silph.validator.*;
import it.uniroma3.siw.silph.Silph.model.*;
import it.uniroma3.siw.silph.Silph.service.*;

@Controller
public class FotografoController {
	
    @Autowired
    private FotografoService fotografoService;
    @Autowired
    private FotografoValidator validator;
    
    @RequestMapping(value = "/fotografo/{id}", method = RequestMethod.GET)
    public String getFotografo(@PathVariable("id") Long id, Model model) {
        model.addAttribute("fotografo", this.fotografoService.findById(id));
    	return "showFotografo";
    }
    
    @RequestMapping("/fotografos")
    public String fotografos(Model model) {
        model.addAttribute("fotografos", this.fotografoService.findAll());
        return "fotografoList";
    }

    @RequestMapping("/funz/addFotografo")
    public String addFotografo(Model model) {
        model.addAttribute("fotografo", new Fotografo());
        return "/funz/fotografoForm";
    }
    
    @RequestMapping(value = "/funz/fotografo", method = RequestMethod.POST)
    public String newFotografo(@Valid @ModelAttribute("fotografo") Fotografo fotografo, 
    									Model model, BindingResult bindingResult) {
        this.validator.validate(fotografo, bindingResult);
        
        if (this.fotografoService.alreadyExists(fotografo)) {
            model.addAttribute("exists", "Fotografo already exists");
            return "/funz/fotografoForm";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.fotografoService.save(fotografo);
                model.addAttribute("fotografos", this.fotografoService.findAll());
                return "fotografoList";
            }
        }
        return "/funz/fotografoForm";
    }

}
