package it.uniroma3.siw.silph.Silph.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.RigaOrdine;
import it.uniroma3.siw.silph.Silph.repository.RigaOrdineRepository;

@Transactional
@Service
public class RigaOrdineService {

	@Autowired
	private RigaOrdineRepository rigaOrdineRepository;

	public RigaOrdine save(RigaOrdine arg0) {
		return rigaOrdineRepository.save(arg0);
	}
	
	public RigaOrdine make() {
		return new RigaOrdine();
	}

	public void delete(RigaOrdine arg0) {
		rigaOrdineRepository.delete(arg0);
	}

	public List<RigaOrdine> findAll() {
		return (List<RigaOrdine>) rigaOrdineRepository.findAll();
	}

	public RigaOrdine findById(Long id) {
		Optional<RigaOrdine> rigaOrdine = this.rigaOrdineRepository.findById(id);
		if (rigaOrdine.isPresent()) 
			return rigaOrdine.get();
		else
			return null;
	}
	
}
