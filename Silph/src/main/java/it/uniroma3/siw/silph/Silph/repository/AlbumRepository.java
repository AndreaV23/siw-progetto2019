package it.uniroma3.siw.silph.Silph.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import it.uniroma3.siw.silph.Silph.model.Album;

public interface AlbumRepository extends JpaRepository<Album, Long> {
	
    List<Album> findByTitolo(String titolo);

}
