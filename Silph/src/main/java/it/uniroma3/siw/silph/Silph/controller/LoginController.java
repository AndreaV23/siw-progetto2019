package it.uniroma3.siw.silph.Silph.controller;


import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The MainController is a Spring Boot Controller to handle
 * the generic interactions with the home pages, and that do not refer to specific entities
 */
@Controller
public class LoginController {

    public LoginController() {
        super();
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/" or "/index".
     * This method prepares and dispatches the home view.
     *
     * @return the name of the home view
     */
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }
    
    @RequestMapping(value = { "/login"}, method = RequestMethod.GET)
    public String login(Model model) {
        return "login";
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/welcome".
     * This method prepares and dispatches the welcome view.
     *
     * @return the name of the welcome view
     */
    @RequestMapping(value = { "/funz/indexLoggato" }, method = RequestMethod.GET)
    public String welcome(HttpSession session, Model model) {
    	if(SecurityContextHolder.getContext().getAuthentication().getPrincipal()!=null) {
    		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    		String role = details.getAuthorities().iterator().next().getAuthority();     // get first authority
    		model.addAttribute("email", details.getUsername());
    		model.addAttribute("role", role);
			session.setAttribute("email", details.getUsername());
			session.setAttribute("role", role);
			session.setAttribute("responsabile", details);
        }

        return "/funz/indexLoggato";
    }

    @RequestMapping(value="/performlogout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }
    
	@GetMapping("/error")
	public String getErrore() {
		return "error";
	}
    
}