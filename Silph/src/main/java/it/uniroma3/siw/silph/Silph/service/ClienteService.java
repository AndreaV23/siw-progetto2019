package it.uniroma3.siw.silph.Silph.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.Cliente;
import it.uniroma3.siw.silph.Silph.repository.ClienteRepository;


@Transactional
@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente save(Cliente arg0) {
		return clienteRepository.save(arg0);
	}

	public void delete(Cliente arg0) {
		clienteRepository.delete(arg0);
	}

	public List<Cliente> findAll() {
		return (List<Cliente>) clienteRepository.findAll();
	}

	public Cliente findByEmail(String email) {
		Cliente cliente;
		if(this.clienteRepository.findByEmail(email).size()>=1) {
			cliente = this.clienteRepository.findByEmail(email).get(0);
			Long id = cliente.getId();
			cliente = this.findById(id);
			return cliente;
		} else {
			return null;
		}
	}

	public Cliente findById(Long id) {
		Optional<Cliente> cliente = this.clienteRepository.findById(id);
		if (cliente.isPresent()) 
			return cliente.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Cliente cliente) {
		List<Cliente> clientes = this.clienteRepository.findByEmailAndNomeAndCognome(cliente.getEmail(), cliente.getNome(), cliente.getCognome());
		if (clientes.size() > 0)
			return true;
		else 
			return false;
	}
	
	
}
