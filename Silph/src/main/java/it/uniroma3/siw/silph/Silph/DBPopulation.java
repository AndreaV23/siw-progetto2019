package it.uniroma3.siw.silph.Silph;


import it.uniroma3.siw.silph.Silph.model.*;
import it.uniroma3.siw.silph.Silph.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import java.io.IOException;

@Component
public class DBPopulation implements ApplicationRunner {

    @Autowired
    private FotografiaRepository fotografiaRepository;
    
    @Autowired
    private FunzionarioRepository funzionarioRepository;

    @Autowired
    private FotografoRepository fotografoRepository;
    
    @Autowired
    private AlbumRepository albumRepository;
    
    @Autowired
    private OrdineRepository ordineRepository;
    
    @Autowired
    private RigaOrdineRepository rigaOrdineRepository;

    public void run(ApplicationArguments args) throws Exception {
        this.deleteAll();
        this.populateDB();
    }

    private void deleteAll() {
        System.out.println("Deleting all fotografias from DB...");
        rigaOrdineRepository.deleteAll();
        fotografiaRepository.deleteAll();
        albumRepository.deleteAll();
        fotografoRepository.deleteAll();
        funzionarioRepository.deleteAll();
        ordineRepository.deleteAll();
        rigaOrdineRepository.deleteAll();
        ordineRepository.deleteAll();
        System.out.println("Done");
    }

    private void populateDB() throws IOException, InterruptedException {
    	
        System.out.println("Storing fotographer...");

        Fotografo ciccioFotografo = new Fotografo("cicciopanza615@gmail.com", "Alessandro", "Sama");
        ciccioFotografo = this.fotografoRepository.save(ciccioFotografo);

        System.out.println("Storing fotografias...");

        Fotografia foto1 = new Fotografia("ciao.jpg", "Ciao (Vespetta)");
        foto1 = this.fotografiaRepository.save(foto1);
                
        Fotografia foto2 = new Fotografia("milan.jpg", "Stemma AC Milan");
        foto2 = this.fotografiaRepository.save(foto2);
        
        Fotografia foto3 = new Fotografia("sh.jpg", "Motorino SH");
        foto3 = this.fotografiaRepository.save(foto3);
        
        Fotografia foto4 = new Fotografia("inter.jpg", "Stemma Inter FC");
        foto4 = this.fotografiaRepository.save(foto4);
        
        Fotografia foto5 = new Fotografia("roma.jpg", "Stemma AS Roma");
        foto5 = this.fotografiaRepository.save(foto5);
        
        System.out.println("Storing admin...");

        Funzionario admin = new Funzionario("andrea.vona23@gmail.com", "Andrea", "Vona");
        String admin1Password = new BCryptPasswordEncoder().encode("adminpass");
        admin.setPassword(admin1Password);
        admin = this.funzionarioRepository.save(admin);
        
        System.out.println("Storing album...");

        Album album = new Album("Motorini");
        Album album2 = new Album("Stemmi Squadre Calcio");
        album = this.albumRepository.save(album);
        album2 = this.albumRepository.save(album2);
        ciccioFotografo.addAlbum(album);
        this.fotografoRepository.save(ciccioFotografo);
        ciccioFotografo.addAlbum(album2);
        album.add(foto3);
        album.add(foto1);
        album2.add(foto2);
        album2.add(foto4);
        album2.add(foto5);
        this.fotografiaRepository.save(foto3);
        this.fotografiaRepository.save(foto1);
        this.fotografiaRepository.save(foto2);
        this.fotografiaRepository.save(foto4);
        this.fotografiaRepository.save(foto5);
        this.fotografoRepository.save(ciccioFotografo);
        album = this.albumRepository.save(album);
        album2 = this.albumRepository.save(album2);
        
        System.out.println("Done.\n");
    }
}
