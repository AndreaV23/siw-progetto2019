package it.uniroma3.siw.silph.Silph.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.Fotografia;
import it.uniroma3.siw.silph.Silph.repository.FotografiaRepository;


@Transactional
@Service
public class FotografiaService {

	@Autowired
	private FotografiaRepository fotografiaRepository;

	public Fotografia save(Fotografia arg0) {
		return fotografiaRepository.save(arg0);
	}

	public void delete(Fotografia arg0) {
		fotografiaRepository.delete(arg0);
	}

	public List<Fotografia> findAll() {
		return (List<Fotografia>) fotografiaRepository.findAll();
	}

	public List<Fotografia> findByTitolo(String titolo) {
		return fotografiaRepository.findByTitolo(titolo);
	}

	public Fotografia findById(Long id) {
		Optional<Fotografia> fotografia = this.fotografiaRepository.findById(id);
		if (fotografia.isPresent()) 
			return fotografia.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Fotografia fotografia) {
		List<Fotografia> fotografias = this.fotografiaRepository.findByTitolo(fotografia.getTitolo());
		if (fotografias.size() > 0)
			return true;
		else 
			return false;
	}	
	
}
