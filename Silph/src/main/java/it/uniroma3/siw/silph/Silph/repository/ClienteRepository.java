package it.uniroma3.siw.silph.Silph.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.silph.Silph.model.*;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    List<Cliente> findByCognome(String cognome);
    List<Cliente> findByNome(String nome);
    List<Cliente> findByEmail(String email);
    List<Cliente> findByEmailAndNomeAndCognome(String email, String nome, String cognome);


}