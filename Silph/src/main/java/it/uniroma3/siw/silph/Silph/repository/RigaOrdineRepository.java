package it.uniroma3.siw.silph.Silph.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.silph.Silph.model.RigaOrdine;

public interface RigaOrdineRepository extends CrudRepository<RigaOrdine, Long> {


}
