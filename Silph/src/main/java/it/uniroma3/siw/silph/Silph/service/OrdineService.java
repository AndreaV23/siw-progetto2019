package it.uniroma3.siw.silph.Silph.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.Fotografia;
import it.uniroma3.siw.silph.Silph.model.Ordine;
import it.uniroma3.siw.silph.Silph.model.RigaOrdine;
import it.uniroma3.siw.silph.Silph.repository.OrdineRepository;
import it.uniroma3.siw.silph.Silph.repository.RigaOrdineRepository;

@Transactional
@Service
public class OrdineService {
	

	@Autowired
	private OrdineRepository ordineRepository;

	public Ordine save(Ordine arg0) {
		return ordineRepository.save(arg0);
	}
	
	public void delete(Ordine arg0) {
		ordineRepository.delete(arg0);
	}

	public List<Ordine> findAll() {
		return (List<Ordine>) ordineRepository.findAll();
	}

	public Ordine findById(Long id) {
		Optional<Ordine> ordine = this.ordineRepository.findById(id);
		if (ordine.isPresent()) 
			return ordine.get();
		else
			return null;
	}
	
}

/*
 * 
 * 	private Ordine ordineAttuale;
 * 	public Ordine aggiungiRigaOrdine(Long idOrdine, Long idFoto) {
		if(this.ordineAttuale==null) {
			this.ordineAttuale = new Ordine();
		}
		RigaOrdine ro = new RigaOrdine(this.ordineAttuale);
		ro = this.rigaOrdineService.save(ro);
		Fotografia foto = this.fotografiaService.findById(idFoto);
		ro.setFotografia(foto);
		ro = this.rigaOrdineService.save(ro);
		this.ordineAttuale.addRigaOrdine(ro);
		return this.ordineAttuale;
	}

	public Ordine getOrdineAttuale() {
		return this.ordineAttuale;
	}
 *  
 *  
 *  */
