package it.uniroma3.siw.silph.Silph.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.silph.Silph.model.Fotografo;
import it.uniroma3.siw.silph.Silph.repository.FotografoRepository;


@Transactional
@Service
public class FotografoService {

	@Autowired
	private FotografoRepository fotografoRepository;

	public Fotografo save(Fotografo arg0) {
		return fotografoRepository.save(arg0);
	}

	public void delete(Fotografo arg0) {
		fotografoRepository.delete(arg0);
	}

	public List<Fotografo> findAll() {
		return (List<Fotografo>) fotografoRepository.findAll();
	}

	public List<Fotografo> findByEmail(String email) {
		return fotografoRepository.findByEmail(email);
	}

	public Fotografo findById(Long id) {
		Optional<Fotografo> fotografo = this.fotografoRepository.findById(id);
		if (fotografo.isPresent()) 
			return fotografo.get();
		else
			return null;
	}
	
	public boolean alreadyExists(Fotografo fotografo) {
		List<Fotografo> fotografos = this.fotografoRepository.findByEmailAndNomeAndCognome(fotografo.getEmail(), fotografo.getNome(), fotografo.getEmail());
		if (fotografos.size() > 0)
			return true;
		else 
			return false;
	}	
	
}
