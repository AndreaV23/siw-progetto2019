package it.uniroma3.siw.silph.Silph.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.silph.Silph.model.Fotografia;

public interface FotografiaRepository extends JpaRepository<Fotografia, Long> {
	
    List<Fotografia> findByTitolo(String titolo);
    List<Fotografia> findByTitoloAndImmagineUrl(String titolo, String immagineUrl);
    
}
