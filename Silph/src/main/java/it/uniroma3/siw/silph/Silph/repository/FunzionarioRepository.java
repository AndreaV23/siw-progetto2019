package it.uniroma3.siw.silph.Silph.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.silph.Silph.model.Funzionario;

public interface FunzionarioRepository extends JpaRepository<Funzionario, Long> {
	
    List<Funzionario> findByEmail(String email);
    List<Funzionario> findByNome(String nome);
    List<Funzionario> findByCognome(String cognome);
    List<Funzionario> findByEmailAndNomeAndCognome(String email, String nome, String cognome);

}
