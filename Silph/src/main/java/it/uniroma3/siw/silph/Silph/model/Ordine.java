package it.uniroma3.siw.silph.Silph.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Ordine {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	private Cliente cliente;
	
	@OneToMany(mappedBy = "ordine")
	private List<RigaOrdine> righeOrdine;

	public Ordine() {}

	public Ordine(Cliente cliente) {
		this.cliente = cliente;
		this.righeOrdine = new ArrayList<RigaOrdine>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<RigaOrdine> getRigheOrdine() {
		return righeOrdine;
	}

	public void setRigheOrdine(List<RigaOrdine> righeOrdine) {
		this.righeOrdine = righeOrdine;
	}

	public boolean addRigaOrdine(RigaOrdine e) {
		return righeOrdine.add(e);
	}
	
	

}
