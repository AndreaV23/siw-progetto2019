package it.uniroma3.siw.silph.Silph.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.silph.Silph.model.*;
import it.uniroma3.siw.silph.Silph.service.*;

@Controller
public class OrdineController {
	
    @Autowired
    private OrdineService ordineService;
    @Autowired
    private RigaOrdineService rigaOrdineService;
    @Autowired
    private FotografiaService fotografiaService;

    
    @RequestMapping("/funz/ordines")
    public String ordines(Model model) {
        model.addAttribute("ordines", this.ordineService.findAll());
        return "funz/ordineList";
    }

    @RequestMapping("/addOrdine")
    public String addOrdine(Model model) {
    	model.addAttribute("fotografias", this.fotografiaService.findAll());
    	Ordine o = new Ordine();
    	RigaOrdine ro = new RigaOrdine();
        model.addAttribute("ordine", o);
        model.addAttribute("rigaordine", ro);
        return "ordineForm";
    }
    
    @RequestMapping(value = "/ordine", method = RequestMethod.POST)
    public String newOrdine(@Valid @ModelAttribute("ordine") Ordine ordine, @Valid @ModelAttribute("rigaordine") RigaOrdine rigaordine, 
    									Model model, BindingResult bindingResult) {
        
        if (!bindingResult.hasErrors()) {
        	rigaordine = this.rigaOrdineService.save(rigaordine); //ordine = 
        	this.ordineService.save(ordine);
        	model.addAttribute("ordine", ordine);
            return "redirect:/clientForm";
        }
        return "ordineForm";
    }

}

/*
 *     	Ordine o;
    	if(idOrdine==-1) {
    		o = new Ordine();
            model.addAttribute("ordine", o);
    	} else {
    		o = this.ordineService.findById(idOrdine);
    	}
        RigaOrdine ro = new RigaOrdine();
        ro = this.rigaOrdineService.save(ro);
        ro = this.rigaOrdineService.findById(idFoto);
    	o.addRigaOrdine(ro);
        ro = this.rigaOrdineService.save(ro);
    	o = this.ordineService.save(o);
        model.addAttribute("fotografia", this.fotografiaService.findById(idFoto));
 * 
 * 
 * */
