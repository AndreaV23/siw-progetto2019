package it.uniroma3.siw.silph.Silph.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.silph.Silph.model.*;

@Component
public class FotografiaValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "titolo", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "immagineUrl", "required");
	    }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Fotografia.class.equals(aClass);
	    }	
}