package it.uniroma3.siw.silph.Silph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilphApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(SilphApplication.class, args);
	}


}
