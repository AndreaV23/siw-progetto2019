package it.uniroma3.siw.silph.Silph.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.silph.Silph.validator.*;
import it.uniroma3.siw.silph.Silph.model.*;
import it.uniroma3.siw.silph.Silph.service.*;

@Controller
public class FotografiaController {
	
    @Autowired
    private FotografiaService fotografiaService;
    @Autowired
    private FotografiaValidator validator;
    @Autowired
    private AlbumService albumService;
    
    
    @RequestMapping(value = "/fotografia/{id}", method = RequestMethod.GET)
    public String getFotografia(@PathVariable("id") Long id, Model model) {
        model.addAttribute("fotografia", this.fotografiaService.findById(id));
    	return "showFotografia";
    }
    
    @RequestMapping("/fotografias")
    public String fotografias(Model model) {
        model.addAttribute("fotografias", this.fotografiaService.findAll());
        return "fotografiaList";
    }

    @RequestMapping("/funz/addFotografia")
    public String addFotografia(Model model) {
        /*UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = details.getAuthorities().iterator().next().getAuthority();     // get first authority
        model.addAttribute("username", details.getUsername());
        model.addAttribute("role", role); */
        model.addAttribute("albums", this.albumService.findAll());
        model.addAttribute("fotografia", new Fotografia());
        return "funz/fotografiaForm";
    }
    
    @RequestMapping(value = "/funz/fotografia", method = RequestMethod.POST)
    public String newFotografia(@Valid @ModelAttribute("fotografia") Fotografia fotografia, 
    									Model model, BindingResult bindingResult) {
    	
        this.validator.validate(fotografia, bindingResult);
        
        if (this.fotografiaService.alreadyExists(fotografia)) {
            model.addAttribute("exists", "Fotografia already exists");
            return "funz/fotografiaForm";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.fotografiaService.save(fotografia);
                fotografia.setFotografo(fotografia.getAlbum().getFotografo());
                this.fotografiaService.save(fotografia);
                model.addAttribute("fotografias", this.fotografiaService.findAll());
                return "fotografiaList";
            }
        }
        return "/funz/fotografiaForm";
    }
    

}
